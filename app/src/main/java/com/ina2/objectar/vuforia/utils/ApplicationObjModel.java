/*===============================================================================
Copyright (c) 2016-2018 PTC, Inc. All Rights Reserved.

Vuforia is a trademark of PTC Inc., registered in the United States and other
countries.
===============================================================================*/

package com.ina2.objectar.vuforia.utils;

import android.content.res.AssetManager;
import android.opengl.GLES20;
import android.opengl.Matrix;
import android.util.Log;

import com.threed.jpct.GenericVertexController;
import com.threed.jpct.Loader;
import com.threed.jpct.Mesh;
import com.threed.jpct.Object3D;
import com.threed.jpct.PolygonManager;
import com.threed.jpct.SimpleVector;
import com.vuforia.Vec3F;
import com.vuforia.Vec4F;
import com.vuforia.Vuforia;

import java.io.IOException;
import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 * Diese Klasse lädt ein Obj-Objekt ohne Texturen.
 * Sie basiert auf der Klasse SampleApplicationV3DModel und wurde entsprechend angepasst.
 */
public class ApplicationObjModel extends MeshObject {

    private static final String LOGTAG = "ObjModel";

    private ByteBuffer _modelVertices;
    private ByteBuffer _modelNormals;
    private int _nbVertices = -1;
    private int _nbFaces = -1;
    private boolean mIsLoaded = false;
    private String modelName = "";

    private final int SHADERS_BUFFER_NUM = 2;
    private final int[] shaderBuffers = new int[SHADERS_BUFFER_NUM];

    /**
     * Liest die obj und mtl Dateien ein.
     * Nur den Namen bzw Pfad angeben, nicht die Dateiendung.
     *
     * @param assetManager
     * @param filename
     * @param offset       Verschiebung, Angaben in der Einheit m.
     * @param rotation     Rotation um die x-Achse in rad.
     * @return
     */
    @SuppressWarnings("ResultOfMethodCallIgnored")
    public boolean loadModel(AssetManager assetManager, String filename,
                             SimpleVector offset, SimpleVector rotation) {
        if (mIsLoaded) {
            unloadModel();
        }

        try {
            modelName = filename;
            //Laden der Obj-Datei
            Object3D[] objArr =
                    Loader.loadOBJ(assetManager.open(filename + ".obj"),
                            assetManager.open(filename + ".mtl"), 0.00001f);
            //Zu einem Object zusammenführen
            Object3D obj = Object3D.mergeAll(objArr);

            obj.rotateX(rotation.x);
            obj.rotateY(rotation.y);
            obj.rotateZ(rotation.z);
            obj.translate(offset);

            obj.compile();
            obj.strip();
            obj.build();
            //MeshController für Vertex und Face info
            Mesh mesh = obj.getMesh();
            GenericVertexController controller =
                    new GenericVertexController() {
                        @Override
                        public void apply() {

                        }
                    };
            mesh.setVertexController(controller, false);
            SimpleVector verte[] = controller.getSourceMesh();
            SimpleVector norm[] = controller.getSourceNormals();
            //PolygonManager enthält Informationen zu den Faces
            PolygonManager polygonManager = obj.getPolygonManager();


            // Read vertices number
            _nbVertices = verte.length;
            Log.d(LOGTAG, "_nbVertices: " + _nbVertices);

            // Read faces number
            _nbFaces = mesh.getTriangleCount();
            Log.d(LOGTAG, "_nbFaces: " + _nbFaces);


            // Read vertices
            _modelVertices = ByteBuffer.allocateDirect(
                    _nbFaces * 3 * 3 * (Float.SIZE / Byte.SIZE));
            _modelVertices.order(ByteOrder.nativeOrder());
            // Read normals
            _modelNormals = ByteBuffer.allocateDirect(
                    _nbFaces * 3 * 3 * (Float.SIZE / Byte.SIZE));
            _modelNormals.order(ByteOrder.nativeOrder());
            for (int i = 0; i < polygonManager.getMaxPolygonID(); i++) {
                //normals hinzufügen ist evt. nicht richtig
                SimpleVector tmpN = polygonManager.getTransformedNormal(i);
                for (int j = 0; j < 3; j++) {
                    //vertex hinzufügen
                    SimpleVector tmpV =
                            polygonManager.getTransformedVertex(i, j);
                    _modelVertices.putFloat(tmpV.x);
                    _modelVertices.putFloat(tmpV.y);
                    _modelVertices.putFloat(tmpV.z);

                    _modelNormals.putFloat(tmpN.x);
                    _modelNormals.putFloat(tmpN.y);
                    _modelNormals.putFloat(tmpN.z);
                }
            }

            _modelVertices.rewind();
            Log.d(LOGTAG, "First vertex: " + _modelVertices.getFloat(0) + "," +
                    _modelVertices.getFloat(1) + "," +
                    _modelVertices.getFloat(2));


            _modelNormals.rewind();
            Log.d(LOGTAG, "First normal: " + _modelNormals.getFloat(0) + "," +
                    _modelNormals.getFloat(1) + "," +
                    _modelNormals.getFloat(2));

            mIsLoaded = true;
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            Log.e(LOGTAG, "Could not load model " + filename);
            return false;
        }
    }

    private void unloadModel() {
        if (!mIsLoaded) {
            return;
        }

        modelName = "";
        mIsLoaded = false;
        _nbVertices = -1;
        _nbFaces = -1;

        _modelVertices = null;
        _modelNormals = null;

        GLES20.glDeleteBuffers(SHADERS_BUFFER_NUM, shaderBuffers, 0);
    }

    public boolean isLoaded() {
        return mIsLoaded;
    }

    public String getModelName() {
        return modelName;
    }


    @Override
    public Buffer getBuffer(BUFFER_TYPE bufferType) {
        Buffer result = null;
        switch (bufferType) {
            case BUFFER_TYPE_VERTEX:
                result = _modelVertices;
                break;
            case BUFFER_TYPE_NORMALS:
                result = _modelNormals;
            default:
                break;
        }
        return result;
    }

    @Override
    public int getNumObjectVertex() {
        return _nbFaces;
    }

    @Override
    public int getNumObjectIndex() {
        return 0;
    }

}
