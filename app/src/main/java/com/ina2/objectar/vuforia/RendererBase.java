/*===============================================================================
Copyright (c) 2019 PTC Inc. All Rights Reserved.

Copyright (c) 2012-2015 Qualcomm Connected Experiences, Inc. All Rights Reserved.

Vuforia is a trademark of PTC Inc., registered in the United States and other
countries.
===============================================================================*/

package com.ina2.objectar.vuforia;

import android.opengl.GLSurfaceView;
import android.util.Log;

import com.ina2.objectar.vuforia.utils.Texture;

import java.util.Vector;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

public class RendererBase implements GLSurfaceView.Renderer
{
    private static final String LOGTAG = "RendererBase";

    protected AppRenderer mAppRenderer;
    protected ApplicationSession vuforiaAppSession;
    protected Vector<Texture> mTextures;


    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config)
    {
        Log.d(LOGTAG, "GLRenderer.onSurfaceCreated");

        // Call Vuforia function to (re)initialize rendering after first use
        // or after OpenGL ES context was lost (e.g. after onPause/onResume):
        vuforiaAppSession.onSurfaceCreated();

        mAppRenderer.onSurfaceCreated();
    }

    
    @Override
    public void onSurfaceChanged(GL10 gl, int width, int height)
    {
        Log.d(LOGTAG, "GLRenderer.onSurfaceChanged");

        // Call Vuforia function to handle render surface size changes:
        vuforiaAppSession.onSurfaceChanged(width, height);

        // RenderingPrimitives to be updated when some rendering change is done
        onConfigurationChanged();
    }


    @Override
    public void onDrawFrame(GL10 gl)
    {
        // Call our function to render content from AppRenderer class
        mAppRenderer.render();
    }


    public void onConfigurationChanged()
    {
        mAppRenderer.onConfigurationChanged();
    }
}
