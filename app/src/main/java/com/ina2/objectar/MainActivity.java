package com.ina2.objectar;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.ina2.objectar.modeltarget.ModelTargets;


public class MainActivity extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        Intent intent = new Intent(this, ModelTargets.class);

        startActivity(intent);
    }
}
