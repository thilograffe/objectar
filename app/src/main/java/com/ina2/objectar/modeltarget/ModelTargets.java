/*===============================================================================
Copyright (c) 2019 PTC Inc. All Rights Reserved.

Copyright (c) 2012-2014 Qualcomm Connected Experiences, Inc. All Rights Reserved.

Vuforia is a trademark of PTC Inc., registered in the United States and other 
countries.
===============================================================================*/

package com.ina2.objectar.modeltarget;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ina2.objectar.R;
import com.ina2.objectar.vuforia.ActivityBase;
import com.ina2.objectar.vuforia.AppMessage;
import com.ina2.objectar.vuforia.ApplicationControl;
import com.ina2.objectar.vuforia.ApplicationException;
import com.ina2.objectar.vuforia.ApplicationSession;
import com.ina2.objectar.vuforia.utils.AppTimer;
import com.ina2.objectar.vuforia.utils.ApplicationGLView;
import com.ina2.objectar.vuforia.utils.LoadingDialogHandler;
import com.threed.jpct.SimpleVector;
import com.vuforia.CameraDevice;
import com.vuforia.DataSet;
import com.vuforia.HINT;
import com.vuforia.ObjectTracker;
import com.vuforia.PositionalDeviceTracker;
import com.vuforia.STORAGE_TYPE;
import com.vuforia.State;
import com.vuforia.TrackableResult;
import com.vuforia.Tracker;
import com.vuforia.TrackerManager;
import com.vuforia.Vuforia;

import java.util.ArrayList;
import java.util.HashMap;


public class ModelTargets extends ActivityBase implements ApplicationControl {
    private static final String LOGTAG = "modelTargets";

    ApplicationSession vuforiaAppSession;

    DataSet currentDataSet;

    public int mCurrentComponentSelectionIndex = 0;

    public ArrayList<Component> components = new ArrayList<>();

    // Our OpenGL view:
    private ApplicationGLView mGlView;

    // Our renderer:
    private ModelTargetRenderer mRenderer;

    // The textures we will use for rendering:
    private HashMap<String, Integer> mSymbolicGuideViewIndices;

    private RelativeLayout mUILayout;
    private Button mBtnLayout;


    ArrayList<View> mSettingsAdditionalViews = new ArrayList<>();

    LoadingDialogHandler loadingDialogHandler = new LoadingDialogHandler(this);

    // Alert Dialog used to display SDK errors
    private AlertDialog mErrorDialog;

    private AppMessage mAppMessage;
    private AppTimer mRelocalizationTimer;
    private AppTimer mStatusDelayTimer;
    private AppTimer skipInstructionStepTimer;

    private ProgressBar next_step_spinner;

    private int mCurrentStatusInfo;

    // Called when the activity first starts or the user navigates back to an
    // activity.
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(LOGTAG, "onCreate");
        super.onCreate(savedInstanceState);

        vuforiaAppSession = new ApplicationSession(this,
                CameraDevice.MODE.MODE_OPTIMIZE_SPEED);

        startLoadingAnimation();

        setViewsReferences();

        // Erstellen der Component-Liste
        components.add(new Component("Welle 1", "models/bauteil_1",
                "modelTargets/Bauteil_1.xml", true));
        components.add(new Component("Box unten", "models/bauteil_1",
                new SimpleVector(0.02f, 0.032f, -0.023f),
                new SimpleVector((float) Math.toRadians(-90.0), 0.0f, 0.0f),
                "modelTargets/box_unten.xml", false));
        components.add(new Component("Welle 4", "models/Bauteil4",
                "modelTargets/Bauteil_4.xml", true));
        components.add(new Component("Baugruppe 1", "models/Bauteil4",
                new SimpleVector(0.044f, 0.021f, -0.0283f),
                new SimpleVector((float) Math.toRadians(-90.0), 0, 0),
                "modelTargets/bauteilgruppe1.xml", false));
        components.add(new Component("Welle 8", "models/Bauteil8",
                "modelTargets/welle8.xml", true));
        components.add(new Component("Baugruppe14", "models/Bauteil8",
                new SimpleVector(0.077f, 0.029f, -0.03f),
                new SimpleVector((float) Math.toRadians(90.0), 0, 0),
                "modelTargets/Baugruppe14.xml", false));
        components.add(new Component("Oberteil", "models/oberteil",
                new SimpleVector(0.070f, 0.028f, 0.020f),
                new SimpleVector(0, (float) Math.toRadians(-90.0), 0),
                "modelTargets/baugruppe148.xml", false));


        vuforiaAppSession
                .initAR(this, ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        // Load any sample specific textures:
        mSymbolicGuideViewIndices = new HashMap<>();

        // Relocalization timer and message
        mAppMessage = new AppMessage(this, mUILayout,
                mUILayout.findViewById(R.id.topbar_layout), false);

        mRelocalizationTimer = new AppTimer(10000, 1000) {
            @Override
            public void onFinish() {
                if (vuforiaAppSession != null) {
                    vuforiaAppSession.resetDeviceTracker();
                }

                super.onFinish();
            }
        };

        mStatusDelayTimer = new AppTimer(1000, 1000) {
            @Override
            public void onFinish() {
                if (mRenderer.isTargetCurrentlyTracked()) {
                    super.onFinish();
                    return;
                }

                if (!mRelocalizationTimer.isRunning()) {
                    mRelocalizationTimer.startTimer();
                }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mAppMessage
                                .show(getString(R.string.instruct_relocalize));
                    }
                });

                super.onFinish();
            }
        };

        skipInstructionStepTimer = new AppTimer(3000, 3000) {
            @Override
            public void onFinish() {
                setSpinnerLayoutVisibilityFalse();
                onNextClick();
                super.onFinish();
            }
        };
    }

    void setViewsReferences() {
        mSettingsAdditionalViews.add(mBtnLayout);
    }

    // Process Single Tap event to trigger autofocus
    private class GestureListener
            extends GestureDetector.SimpleOnGestureListener {
        // Used to set autofocus one second after a manual focus is triggered
        private final Handler autofocusHandler = new Handler();


        @Override
        public boolean onDown(MotionEvent e) {
            return true;
        }


        @Override
        public boolean onSingleTapUp(MotionEvent e) {
            // Generates a Handler to trigger autofocus
            // after 1 second
            autofocusHandler.postDelayed(new Runnable() {
                public void run() {
                    boolean result = CameraDevice.getInstance().setFocusMode(
                            CameraDevice.FOCUS_MODE.FOCUS_MODE_CONTINUOUSAUTO);

                    if (!result) {
                        Log.e("SingleTapUp", "Unable to trigger focus");
                    }
                }
            }, 1000L);

            return true;
        }
    }


    // Called when the activity will start interacting with the user.
    @Override
    protected void onResume() {
        Log.d(LOGTAG, "onResume");
        super.onResume();

        showProgressIndicator(true);

        vuforiaAppSession.onResume();
    }


    // Callback for configuration changes the activity handles itself
    @Override
    public void onConfigurationChanged(Configuration config) {
        Log.d(LOGTAG, "onConfigurationChanged");
        super.onConfigurationChanged(config);

        vuforiaAppSession.onConfigurationChanged();

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (mRenderer != null) {
                    mRenderer.updateRenderingPrimitives();
                }

                showProgressIndicator(false);
            }
        }, 100);
    }


    // Called when the system is about to start resuming a previous activity.
    @Override
    protected void onPause() {
        Log.d(LOGTAG, "onPause");
        super.onPause();

        if (mGlView != null) {
            mGlView.setVisibility(View.INVISIBLE);
            mGlView.onPause();
        }

        vuforiaAppSession.onPause();
    }


    // The final call you receive before your activity is destroyed.
    @Override
    protected void onDestroy() {
        Log.d(LOGTAG, "onDestroy");
        super.onDestroy();

        try {
            vuforiaAppSession.stopAR();
        } catch (ApplicationException e) {
            Log.e(LOGTAG, e.getString());
        }

        // Unload texture:

        mSymbolicGuideViewIndices.clear();
        mSymbolicGuideViewIndices = null;

        System.gc();
    }


    // Initializes AR application components.
    private void initApplicationAR() {
        // Create OpenGL ES view:
        int depthSize = 16;
        int stencilSize = 0;
        boolean translucent = Vuforia.requiresAlpha();

        mGlView = new ApplicationGLView(this);
        mGlView.init(translucent, depthSize, stencilSize);

        mRenderer = new ModelTargetRenderer(this, vuforiaAppSession);

        mGlView.setRenderer(mRenderer);
        mGlView.setPreserveEGLContextOnPause(true);

        setRendererReference(mRenderer);
    }


    private void startLoadingAnimation() {
        mUILayout = (RelativeLayout) View
                .inflate(this, R.layout.camera_overlay_model_targets, null);

        mUILayout.setVisibility(View.VISIBLE);
        mUILayout.setBackgroundColor(Color.BLACK);

        mBtnLayout = mUILayout.findViewById(R.id.reset_btn);

        RelativeLayout topbarLayout =
                mUILayout.findViewById(R.id.topbar_layout);
        topbarLayout.setVisibility(View.VISIBLE);

        TextView title = mUILayout.findViewById(R.id.topbar_title);
        title.setText(getText(R.string.feature_model_targets));

        mSettingsAdditionalViews.add(topbarLayout);

        // Gets a reference to the loading dialog
        loadingDialogHandler.mLoadingDialogContainer =
                mUILayout.findViewById(R.id.loading_indicator);

        // Shows the loading indicator at start
        loadingDialogHandler
                .sendEmptyMessage(LoadingDialogHandler.SHOW_LOADING_DIALOG);

        next_step_spinner = mUILayout.findViewById(R.id.loading_next_target);
        setSpinnerLayoutVisibilityFalse();

        // Adds the inflated layout to the view
        addContentView(mUILayout, new LayoutParams(LayoutParams.MATCH_PARENT,
                LayoutParams.MATCH_PARENT));

    }

    public void setSpinnerLayoutVisibilityFalse() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                next_step_spinner.setVisibility(View.INVISIBLE);
            }
        });
    }

    public void setSpinnerLayoutVisibilityTrue() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                next_step_spinner.setVisibility(View.VISIBLE);
            }
        });
    }


    private void onNextClick() {
        if (mCurrentComponentSelectionIndex < components.size() - 1) {
            mCurrentComponentSelectionIndex++;

            if (!doChangeTrackersData(
                    components.get(mCurrentComponentSelectionIndex))) {
                mCurrentComponentSelectionIndex--;
            }
        }
    }


    public void skipToNextInstructionStep(String targetName) {
        Component currentComponent =
                components.get(mCurrentComponentSelectionIndex);

        if (currentComponent.isAutoSkip() &&
                !skipInstructionStepTimer.isRunning() &&
                currentComponent.getDataSet().getTrackables().at(0).getName()
                        .equals(targetName)) {
            runOnUiThread(() -> showToast("Please wait!"));
            setSpinnerLayoutVisibilityTrue();
            skipInstructionStepTimer.startTimer();
        }
    }

    /*private void checkTrackersData() {
        if (!mTargetTracked) {
            showToast("Could not track current instruction step! Please try again.");
            mRenderer.color = new Vec3F(0.01f, 0.48f, 0.9f);

            if (mCurrentComponentSelectionIndex > 0) mCurrentComponentSelectionIndex -= 1;
            doChangeTrackersData(mCurrentComponentSelectionIndex);
        } else {
            mTargetTracked = false;
            mRenderer.color = new Vec3F(0.24f, 0.76f, 0.18f);
        }
    }*/


    // Method for changing the tracker's data.
    // index represents the dataset, which should get tracked
    private boolean doChangeTrackersData(Component component) {
        boolean result = false;
        DataSet dataSet = component.getDataSet();

        TrackerManager tManager = TrackerManager.getInstance();
        ObjectTracker objectTracker = (ObjectTracker) tManager
                .getTracker(ObjectTracker.getClassType());

        if (objectTracker == null) {
            return false;
        }

        if (currentDataSet != null) {
            objectTracker.deactivateDataSet(currentDataSet);
        }

        if (dataSet != null) {
            if (mRenderer != null) {
                mRenderer.loadModel();
            }
            result = objectTracker.activateDataSet(dataSet);

            if (result) {
                currentDataSet = dataSet;
                ((TextView) (findViewById(R.id.topbar_title)))
                        .setText(component.getName());
            }
        }

        return result;
    }


    // Methods to load and destroy tracking data.
    @Override
    public boolean doLoadTrackersData() {
        TrackerManager tManager = TrackerManager.getInstance();
        ObjectTracker objectTracker = (ObjectTracker) tManager
                .getTracker(ObjectTracker.getClassType());

        if (objectTracker == null) {
            return false;
        }


        for (Component component : components) {
            component.setDataSet(objectTracker.createDataSet());
            if (component.getDataSet() == null) {
                return false;
            }
            if (!component.getDataSet().load(component.getTargetPath(),
                    STORAGE_TYPE.STORAGE_APPRESOURCE)) {
                return false;
            }
        }

        if (!doChangeTrackersData(
                components.get(mCurrentComponentSelectionIndex))) {
            return false;
        }

        return true;
    }


    @Override
    public boolean doUnloadTrackersData() {
        // To return true if the trackers were unloaded correctly
        boolean result = true;

        TrackerManager tManager = TrackerManager.getInstance();
        ObjectTracker objectTracker = (ObjectTracker) tManager
                .getTracker(ObjectTracker.getClassType());
        if (objectTracker == null) {
            return false;
        }

        for (Component component : components) {
            DataSet tmpDataSet = component.getDataSet();
            if (component != null) {
                objectTracker.deactivateDataSet(tmpDataSet);
                objectTracker.destroyDataSet(tmpDataSet);
                component.setDataSet(null);
            }
        }

        currentDataSet = null;
        mCurrentComponentSelectionIndex = 0;

        return result;
    }


    @Override
    public void onInitARDone(ApplicationException exception) {
        if (exception == null) {
            initApplicationAR();

            mRenderer.setActive(true);

            // Now add the GL surface view. It is important
            // that the OpenGL ES surface view gets added
            // BEFORE the camera is started and video
            // background is configured.
            addContentView(mGlView, new LayoutParams(LayoutParams.MATCH_PARENT,
                    LayoutParams.MATCH_PARENT));

            // Sets the UILayout to be drawn in front of the camera
            mUILayout.bringToFront();

            // Sets the layout background to transparent
            mUILayout.setBackgroundColor(Color.TRANSPARENT);

            Button resetBtn = mUILayout.findViewById(R.id.reset_btn);

            resetBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    resetTrackers();
                }
            });

            resetBtn.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    mCurrentComponentSelectionIndex = 0;
                    doChangeTrackersData(
                            components.get(mCurrentComponentSelectionIndex));

                    return true;
                }
            });

            Button nextBtn = mUILayout.findViewById(R.id.next_btn);

            nextBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onNextClick();
                }
            });

            if (mRenderer.areModelsLoaded()) {
                showProgressIndicator(false);
            }

            vuforiaAppSession.startAR();


        } else {
            Log.e(LOGTAG, exception.getString());
            showInitializationErrorMessage(exception.getString());
        }

    }

    public void resetTrackers() {
        TrackerManager trackerManager = TrackerManager.getInstance();
        ObjectTracker objectTracker = (ObjectTracker) trackerManager.
                getTracker(ObjectTracker.getClassType());
        PositionalDeviceTracker deviceTracker =
                (PositionalDeviceTracker) trackerManager.
                        getTracker(PositionalDeviceTracker.getClassType());

        if (deviceTracker != null) {
            deviceTracker.reset();
        }

        // Reset dataset
        if (objectTracker != null) {
            objectTracker.stop();


            if (currentDataSet != null && currentDataSet.isActive()) {
                objectTracker.deactivateDataSet(currentDataSet);
                objectTracker.activateDataSet(currentDataSet);
            }


            objectTracker.start();
        }
    }

    public void showProgressIndicator(boolean show) {
        if (loadingDialogHandler != null) {
            if (show) {
                loadingDialogHandler.sendEmptyMessage(
                        LoadingDialogHandler.SHOW_LOADING_DIALOG);
            } else {
                loadingDialogHandler.sendEmptyMessage(
                        LoadingDialogHandler.HIDE_LOADING_DIALOG);
            }
        }
    }


    // Shows initialization error messages as System dialogs
    public void showInitializationErrorMessage(String message) {
        final String errorMessage = message;
        runOnUiThread(new Runnable() {
            public void run() {
                if (mErrorDialog != null) {
                    mErrorDialog.dismiss();
                }

                // Generates an Alert Dialog to show the error message
                AlertDialog.Builder builder =
                        new AlertDialog.Builder(ModelTargets.this);
                builder.setMessage(errorMessage)
                        .setTitle(getString(R.string.INIT_ERROR))
                        .setCancelable(false).setIcon(0)
                        .setPositiveButton(getString(R.string.button_OK),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {
                                        finish();
                                    }
                                });

                mErrorDialog = builder.create();
                mErrorDialog.show();
            }
        });
    }


    @Override
    public void onVuforiaUpdate(State state) {
    }

    @Override
    public void onVuforiaResumed() {
        if (mGlView != null) {
            mGlView.setVisibility(View.VISIBLE);
            mGlView.onResume();
        }
    }

    @Override
    public void onVuforiaStarted() {
        mRenderer.updateRenderingPrimitives();

        // Set camera focus mode
        Log.d(LOGTAG, "Continousauto MODE");
        if (!CameraDevice.getInstance().setFocusMode(
                CameraDevice.FOCUS_MODE.FOCUS_MODE_CONTINUOUSAUTO)) {
            Log.d(LOGTAG, "Triggeredauto MODE");
            // If continuous autofocus mode fails, attempt to set to a different mode
            if (!CameraDevice.getInstance().setFocusMode(
                    CameraDevice.FOCUS_MODE.FOCUS_MODE_TRIGGERAUTO)) {
                Log.d(LOGTAG, "normal MODE");
                CameraDevice.getInstance().setFocusMode(
                        CameraDevice.FOCUS_MODE.FOCUS_MODE_NORMAL);
            }
        }

        /*if (!CameraDevice.getInstance().setFlashTorchMode(true)) {
            Log.d(LOGTAG, "Flash could not be initialized");
        }*/

        if (mRenderer.areModelsLoaded()) {
            showProgressIndicator(false);
        }
    }


    @Override
    public boolean doInitTrackers() {
        // Indicate if the trackers were initialized correctly
        boolean result = true;

        TrackerManager trackerManager = TrackerManager.getInstance();

        // Initialize the object tracker:
        Tracker tracker =
                trackerManager.initTracker(ObjectTracker.getClassType());

        if (tracker == null) {
            Log.d(LOGTAG, "Failed to initialize ObjectTracker.");
            result = false;
        } else {
            Log.d(LOGTAG, "Successfully initialized ObjectTracker.");
        }

        Vuforia.setHint(HINT.HINT_MODEL_TARGET_RECO_WHILE_EXTENDED_TRACKED, 1);

        // Initialize the Positional Device Tracker
        PositionalDeviceTracker deviceTracker =
                (PositionalDeviceTracker) trackerManager
                        .initTracker(PositionalDeviceTracker.getClassType());

        if (deviceTracker != null) {
            Log.i(LOGTAG, "Successfully initialized Device Tracker");
        } else {
            Log.e(LOGTAG, "Failed to initialize Device Tracker");
        }
        return result;
    }


    @Override
    public boolean doStartTrackers() {
        // Indicate if the trackers were started correctly
        boolean result = true;

        Tracker objectTracker = TrackerManager.getInstance()
                .getTracker(ObjectTracker.getClassType());
        if (objectTracker != null) {
            objectTracker.start();
        }

        return setDeviceTrackerEnabled(true);
    }


    @Override
    public boolean doStopTrackers() {
        // Returns true if the trackers were stopped correctly
        Tracker objectTracker = TrackerManager.getInstance()
                .getTracker(ObjectTracker.getClassType());
        if (objectTracker != null) {
            objectTracker.stop();
        }

        return setDeviceTrackerEnabled(false);
    }

    boolean setDeviceTrackerEnabled(boolean enabled) {
        boolean result = true;

        Tracker deviceTracker = TrackerManager.getInstance()
                .getTracker(PositionalDeviceTracker.getClassType());

        if (deviceTracker == null) {
            Log.e(LOGTAG, "ERROR: Could not toggle device tracker state");
            return false;
        }

        if (enabled) {
            if (deviceTracker.start()) {
                Log.d(LOGTAG, "Successfully started device tracker");
            } else {
                result = false;
                Log.e(LOGTAG, "Failed to start device tracker");
            }
        } else {
            deviceTracker.stop();
            Log.d(LOGTAG, "Successfully stopped device tracker");
        }

        return result;
    }


    @Override
    public boolean doDeinitTrackers() {
        // Returns true if the trackers were deinitialized correctly
        boolean result;

        TrackerManager tManager = TrackerManager.getInstance();
        result = tManager.deinitTracker(ObjectTracker.getClassType());
        result = result &&
                tManager.deinitTracker(PositionalDeviceTracker.getClassType());

        return result;
    }


    private final static int CMD_BACK = -1;
    private final static int CMD_AUTOFOCUS = 0;
    private final static int CMD_NEXT_GUIDEVIEW = 1;
    private final static int CMD_DATASET_START_INDEX = 2;


    // Helper method to avoid code duplication
    /*private ModelTarget getModelTarget()
    {
        // assuming there is only one CAD trackable in dataSetOt1
        ModelTarget modelTarget = null;
        for(int i = 0; i < currentDataSet.getTrackables().size(); ++i)
        {
            Trackable trackable = currentDataSet.getTrackables().at(i);
            if (trackable instanceof ModelTarget)
            {
                modelTarget = (ModelTarget)trackable;
                break;
            }
        }
        return modelTarget;
    }*/


    private void showToast(String text) {
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
    }

    public void setBtnLayoutVisibility(int visibility) {
        mBtnLayout.setVisibility(visibility);
    }


    public void checkForRelocalization(final int statusInfo) {
        if (mCurrentStatusInfo == statusInfo) {
            return;
        }

        mCurrentStatusInfo = statusInfo;

        if (mCurrentStatusInfo == TrackableResult.STATUS_INFO.RELOCALIZING) {
            // If the status is RELOCALIZING, start the timer
            if (!mStatusDelayTimer.isRunning()) {
                mStatusDelayTimer.startTimer();
            }
        } else {
            // If the status is not RELOCALIZING, stop the timers and hide the message
            if (mStatusDelayTimer.isRunning()) {
                mStatusDelayTimer.stopTimer();
            }

            if (mRelocalizationTimer.isRunning()) {
                mRelocalizationTimer.stopTimer();
            }

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mAppMessage.hide();
                }
            });
        }
    }
}
