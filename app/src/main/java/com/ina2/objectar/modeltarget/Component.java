package com.ina2.objectar.modeltarget;

import com.threed.jpct.SimpleVector;
import com.vuforia.DataSet;

public class Component {
    private String name;
    private String renderObjPath;
    private SimpleVector offset;
    private DataSet dataSet;
    private SimpleVector rotation;
    private String targetPath;
    private boolean autoSkip;

    public Component(String name, String renderObjPath, String targetPath,
                     boolean autoSkip) {
        this.name = name;
        this.renderObjPath = renderObjPath;
        this.targetPath = targetPath;
        this.offset = new SimpleVector(0, 0, 0);
        this.rotation = new SimpleVector(0, 0, 0);
        this.autoSkip = autoSkip;
    }

    public Component(String name, String renderObjPath, SimpleVector offset,
                     SimpleVector rotation, String targetPath,
                     boolean autoSkip) {
        this.name = name;
        this.renderObjPath = renderObjPath;
        this.offset = offset;
        this.rotation = rotation;
        this.targetPath = targetPath;
        this.autoSkip = autoSkip;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRenderObjPath() {
        return renderObjPath;
    }

    public void setRenderObjPath(String renderObjPath) {
        this.renderObjPath = renderObjPath;
    }

    public SimpleVector getOffset() {
        return offset;
    }

    public void setOffset(SimpleVector offset) {
        this.offset = offset;
    }

    public SimpleVector getRotation() {
        return rotation;
    }

    public void setRotation(SimpleVector rotation) {
        this.rotation = rotation;
    }

    public String getTargetPath() {
        return targetPath;
    }

    public void setTargetPath(String targetPath) {
        this.targetPath = targetPath;
    }

    public DataSet getDataSet() {
        return dataSet;
    }

    public void setDataSet(DataSet dataSet) {
        this.dataSet = dataSet;
    }


    public boolean isAutoSkip() {
        return autoSkip;
    }

    public void setAutoSkip(boolean autoSkip) {
        this.autoSkip = autoSkip;
    }
}
