/*===============================================================================
Copyright (c) 2019 PTC Inc. All Rights Reserved.

Copyright (c) 2012-2014 Qualcomm Connected Experiences, Inc. All Rights Reserved.

Vuforia is a trademark of PTC Inc., registered in the United States and other 
countries.
===============================================================================*/

package com.ina2.objectar.modeltarget;

import android.opengl.GLES20;
import android.opengl.Matrix;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;

import com.ina2.objectar.vuforia.AppRenderer;
import com.ina2.objectar.vuforia.AppRendererControl;
import com.ina2.objectar.vuforia.ApplicationSession;
import com.ina2.objectar.vuforia.RendererBase;
import com.ina2.objectar.vuforia.utils.ApplicationObjModel;
import com.ina2.objectar.vuforia.utils.LightingShaders;
import com.ina2.objectar.vuforia.utils.Plane;
import com.ina2.objectar.vuforia.utils.VuforiaMath;
import com.ina2.objectar.vuforia.utils.VuforiaUtils;
import com.vuforia.DeviceTrackableResult;
import com.vuforia.GuideView;
import com.vuforia.Image;
import com.vuforia.Matrix44F;
import com.vuforia.ModelTarget;
import com.vuforia.ModelTargetResult;
import com.vuforia.State;
import com.vuforia.Tool;
import com.vuforia.TrackableResult;
import com.vuforia.TrackableResultList;
import com.vuforia.Vec2F;
import com.vuforia.Vec3F;
import com.vuforia.Vec4F;
import com.vuforia.Vuforia;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;

import javax.microedition.khronos.opengles.GL10;


// The renderer class for the modelTargets sample.
public class ModelTargetRenderer extends RendererBase
        implements AppRendererControl {
    private static final String LOGTAG = "ModelTargetRenderer";

    private ModelTargets mActivity;

    private int planeShaderProgramID;
    private int planeVertexHandle;
    private int planeTextureCoordHandle;
    private int planeMvpMatrixHandle;
    private int planeTexSampler2DHandle;
    private int planeColorHandle;

    private float mPlaneWidth;
    private float mPlaneHeight;
    private float transparency = 1.0f;

    private int guideViewHandle;
    private Vec2F mGuideViewScale;


    private int shaderProgramID;
    private int vertexHandle;
    private int transparencyHandle;
    private int mvpMatrixHandle;
    private int mvMatrixHandle;
    private int normalHandle;
    private int textureCoordHandle;
    private int texSampler2DHandle;
    private int normalMatrixHandle;
    private int lightPositionHandle;
    private int lightColorHandle;
    private int colorCorrectionHandle;
    private int intensityCorrectionHandle;
    private int colorHandle;
    public Vec3F color = new Vec3F(0.01f, 0.48f, 0.9f);

    // No color correction by default
    private Vec4F mColorCorrection = new Vec4F(1.0f, 1.0f, 1.0f, 1.0f);
    private float mIntensityCorrection = 1.0f;

    private Plane mPlane;
    private ApplicationObjModel objectToRender;

    private boolean mAreModelsLoaded = false;

    private boolean mIsTargetCurrentlyTracked = false;
    private boolean mIsKTMCurrentModel = false;

    private HashMap<String, Integer> mSymbolicGuideViewIndices;

    private String mCurrentModelTargetId;
    private int mActiveGuideViewIndex = -1;

    ModelTargetRenderer(ModelTargets activity, ApplicationSession session) {
        mActivity = activity;
        vuforiaAppSession = session;
        // AppRenderer used to encapsulate the use of RenderingPrimitives setting
        // the device mode AR/VR and stereo mode
        mAppRenderer = new AppRenderer(this, mActivity,
                vuforiaAppSession.getVideoMode(), 0.01f, 5f);

        guideViewHandle = -1;
    }


    @Override
    public void onSurfaceChanged(GL10 gl, int width, int height) {
        super.onSurfaceChanged(gl, width, height);

        // Invalidate guide view handle so it is regenerated onSurfaceChanged()
        guideViewHandle = -1;
        mCurrentModelTargetId = null;
    }


    public void setActive(boolean active) {
        mAppRenderer.setActive(active);
    }


    // Function for initializing the renderer.
    @Override
    public void initRendering() {
        objectToRender = new ApplicationObjModel();

        GLES20.glClearColor(0.0f, 0.0f, 0.0f,
                Vuforia.requiresAlpha() ? 0.0f : 1.0f);

        shaderProgramID = VuforiaUtils.createProgramFromShaderSrc(
                LightingShaders.LIGHTING_VERTEX_SHADER,
                LightingShaders.LIGHTING_FRAGMENT_SHADER);

        if (shaderProgramID > 0) {
            vertexHandle = GLES20.glGetAttribLocation(shaderProgramID,
                    "vertexPosition");
            normalHandle = GLES20.glGetAttribLocation(shaderProgramID,
                    "vertexNormal");
            mvpMatrixHandle = GLES20.glGetUniformLocation(shaderProgramID,
                    "u_mvpMatrix");
            mvMatrixHandle =
                    GLES20.glGetUniformLocation(shaderProgramID, "u_mvMatrix");
            normalMatrixHandle = GLES20.glGetUniformLocation(shaderProgramID,
                    "u_normalMatrix");
            lightPositionHandle =
                    GLES20.glGetUniformLocation(shaderProgramID, "u_lightPos");
            lightColorHandle = GLES20.glGetUniformLocation(shaderProgramID,
                    "u_lightColor");
            colorCorrectionHandle =
                    GLES20.glGetUniformLocation(shaderProgramID,
                            "u_colorCorrection");
            intensityCorrectionHandle =
                    GLES20.glGetUniformLocation(shaderProgramID,
                            "u_intensityCorrection");
            transparencyHandle = GLES20.glGetUniformLocation(shaderProgramID,
                    "u_transparency");
            transparency = 0.5f;
            colorHandle =
                    GLES20.glGetUniformLocation(shaderProgramID, "u_color");
        } else {
            Log.e(LOGTAG, "Could not init lighting shader");
        }

        if (!mAreModelsLoaded) {
            loadModel();
        }

        mGuideViewScale = new Vec2F(1.0f, 1.0f);
    }

    public void loadModel() {
        LoadModelTask modelTask = new LoadModelTask(this);
        modelTask.execute();
    }

    public boolean loadApplicationModel(Component component) {
        return objectToRender.loadModel(mActivity.getResources().getAssets(),
                component.getRenderObjPath(), component.getOffset(),
                component.getRotation());
    }


    public void updateRenderingPrimitives() {
        mAppRenderer.updateRenderingPrimitives();
    }


    private static class LoadModelTask
            extends AsyncTask<Void, Integer, Boolean> {
        private final WeakReference<ModelTargetRenderer> mRendererRef;

        LoadModelTask(ModelTargetRenderer mtRenderer) {
            mRendererRef = new WeakReference<>(mtRenderer);
        }

        protected Boolean doInBackground(Void... params) {
            ModelTargetRenderer renderer = mRendererRef.get();
            ModelTargets activity = renderer.mActivity;

            renderer.objectToRender = new ApplicationObjModel();
            Component component = activity.components
                    .get(activity.mCurrentComponentSelectionIndex);
            boolean landerLoaded = renderer.objectToRender
                    .loadModel(activity.getResources().getAssets(),
                            component.getRenderObjPath(),
                            component.getOffset(), component.getRotation());

            renderer.mAreModelsLoaded = landerLoaded;

            return renderer.mAreModelsLoaded;
        }

        protected void onPostExecute(Boolean result) {
            ModelTargets activity = mRendererRef.get().mActivity;

            // Hide the Loading Dialog
            activity.showProgressIndicator(false);
            activity.setBtnLayoutVisibility(View.VISIBLE);
        }
    }


    // The render function called from SampleAppRendering by using RenderingPrimitives views.
    // The state is owned by AppRenderer which is controlling it's lifecycle.
    // State should not be cached outside this method.
    public void renderFrame(State state, float[] projectionMatrix) {
        // Renders video background replacing Renderer.DrawVideoBackground()
        mAppRenderer.renderVideoBackground();

        GLES20.glEnable(GLES20.GL_DEPTH_TEST);

        GLES20.glEnable(GLES20.GL_CULL_FACE);
        GLES20.glEnable(GLES20.GL_BLEND);
        GLES20.glBlendFunc(GLES20.GL_SRC_ALPHA, GLES20.GL_ONE_MINUS_SRC_ALPHA);
        GLES20.glCullFace(GLES20.GL_BACK);

        if (state.getDeviceTrackableResult() != null) {
            int statusInfo = state.getDeviceTrackableResult().getStatusInfo();
            mActivity.checkForRelocalization(statusInfo);
        }

        GuideView activeGuideView;
        Image textureImage;

        // Set the device pose matrix as identity
        Matrix44F devicePoseMatrix = VuforiaMath.Matrix44FIdentity();
        Matrix44F modelMatrix;

        // Read device pose from the state and create a corresponding view matrix (inverse of the device pose)
        if (state.getDeviceTrackableResult() != null &&
                state.getDeviceTrackableResult().getStatus() !=
                        TrackableResult.STATUS.NO_POSE) {
            modelMatrix = Tool.convertPose2GLMatrix(
                    state.getDeviceTrackableResult().getPose());

            // We transpose here because Matrix44FInverse returns a transposed matrix
            devicePoseMatrix = VuforiaMath.Matrix44FTranspose(
                    VuforiaMath.Matrix44FInverse(modelMatrix));
        }

        TrackableResult currentTrackableResult;
        TrackableResultList trackableResultList = state.getTrackableResults();

        // Determine if target is currently being tracked
        setIsTargetCurrentlyTracked(trackableResultList);


        ArrayList<ModelTarget> modelsBeingSearchedFor = new ArrayList<>();
        ModelTarget modelRequiringGuidance = null;

        // Iterate through trackable results and render any augmentations
        for (TrackableResult trackableResult : trackableResultList) {
            if (trackableResult.isOfType(ModelTargetResult.getClassType())) {
                ModelTarget modelTarget =
                        (ModelTarget) trackableResult.getTrackable();

                if (trackableResult.getStatus() ==
                        TrackableResult.STATUS.TRACKED ||
                        trackableResult.getStatus() ==
                                TrackableResult.STATUS.EXTENDED_TRACKED) {
                    currentTrackableResult = trackableResult;
                    String trackableName =
                            currentTrackableResult.getTrackable().getName();


                    modelMatrix = Tool.convertPose2GLMatrix(
                            currentTrackableResult.getPose());
                    renderModel(projectionMatrix, devicePoseMatrix.getData(),
                            modelMatrix.getData());

                    VuforiaUtils.checkGLError("renderFrame(), tracking");

                } else if (trackableResult.getStatusInfo() ==
                        TrackableResult.STATUS_INFO.NO_DETECTION_RECOMMENDING_GUIDANCE) {
                    modelRequiringGuidance = modelTarget;
                } else if (trackableResult.getStatusInfo() ==
                        TrackableResult.STATUS_INFO.INITIALIZING) {
                    modelsBeingSearchedFor.add(modelTarget);
                }
            }
        }

        // If we are not tracking any models, see if any models are initializing


        GLES20.glDisable(GLES20.GL_BLEND);
        GLES20.glDisable(GLES20.GL_DEPTH_TEST);
    }

    private void renderModel(float[] projectionMatrix, float[] viewMatrix,
                             float[] modelMatrix) {


        ApplicationObjModel currentModel = objectToRender;

        if (!currentModel.isLoaded()) {
            return;
        }

        float[] modelViewProjection = new float[16];

        // Combine device pose (view matrix) with model matrix
        Matrix.multiplyMM(modelMatrix, 0, viewMatrix, 0, modelMatrix, 0);

        // Do the final combination with the projection matrix
        Matrix.multiplyMM(modelViewProjection, 0, projectionMatrix, 0,
                modelMatrix, 0);

        // activate the shader program and bind the vertex/normal/tex coords
        GLES20.glUseProgram(shaderProgramID);

        GLES20.glDisable(GLES20.GL_CULL_FACE);

        GLES20.glVertexAttribPointer(vertexHandle, 3, GLES20.GL_FLOAT, false,
                0, currentModel.getVertices());
        GLES20.glVertexAttribPointer(normalHandle, 3, GLES20.GL_FLOAT, false,
                0, currentModel.getNormals());


        GLES20.glEnableVertexAttribArray(vertexHandle);
        GLES20.glEnableVertexAttribArray(normalHandle);


        GLES20.glUniformMatrix4fv(mvpMatrixHandle, 1, false,
                modelViewProjection, 0);
        GLES20.glUniformMatrix4fv(mvMatrixHandle, 1, false, modelMatrix, 0);

        float[] inverseMatrix = new float[16];
        Matrix.invertM(inverseMatrix, 0, modelMatrix, 0);

        float[] normalMatrix = new float[16];
        Matrix.transposeM(normalMatrix, 0, inverseMatrix, 0);

        GLES20.glUniformMatrix4fv(normalMatrixHandle, 1, false, normalMatrix,
                0);

        GLES20.glUniform4f(lightPositionHandle, 0.2f, -1.0f, 0.5f, -1.0f);
        GLES20.glUniform4f(lightColorHandle, 0.5f, 0.5f, 0.5f, 1.0f);
        GLES20.glUniform4f(colorCorrectionHandle,
                mColorCorrection.getData()[0], mColorCorrection.getData()[1],
                mColorCorrection.getData()[2], mColorCorrection.getData()[3]);
        GLES20.glUniform1f(intensityCorrectionHandle, mIntensityCorrection);
        GLES20.glUniform3f(colorHandle, color.getData()[0], color.getData()[1],
                color.getData()[2]);
        if (transparency < 1.0 && transparency >= 0.0f) {
            GLES20.glEnable(GLES20.GL_BLEND);
            GLES20.glBlendFunc(GLES20.GL_SRC_ALPHA,
                    GLES20.GL_ONE_MINUS_SRC_ALPHA);

        } else {
            transparency = 1.0f;
        }
        GLES20.glUniform1f(transparencyHandle, transparency);

        GLES20.glDrawArrays(GLES20.GL_TRIANGLES, 0,
                currentModel.getNumObjectVertex() * 3);

        GLES20.glDisableVertexAttribArray(vertexHandle);
        GLES20.glDisableVertexAttribArray(normalHandle);
        GLES20.glDisable(GLES20.GL_BLEND);
    }


    boolean areModelsLoaded() {
        return mAreModelsLoaded;
    }


    private void setIsTargetCurrentlyTracked(
            TrackableResultList trackableResultList) {
        for (TrackableResult result : trackableResultList) {
            // Check the tracking status for result types
            // other than DeviceTrackableResult. ie: ModelTargetResult
            if (!result.isOfType(DeviceTrackableResult.getClassType())) {
                int currentStatus = result.getStatus();
                int currentStatusInfo = result.getStatusInfo();

                // The target is currently being tracked if the status is TRACKED|NORMAL
                if (currentStatus == TrackableResult.STATUS.TRACKED ||
                        currentStatusInfo ==
                                TrackableResult.STATUS_INFO.NORMAL) {
                    mIsTargetCurrentlyTracked = true;

                    mActivity.skipToNextInstructionStep(result.getTrackable().getName());

                    return;
                }
            }
        }

        mIsTargetCurrentlyTracked = false;
    }


    boolean isTargetCurrentlyTracked() {
        return mIsTargetCurrentlyTracked;
    }
}
